/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, {useState} from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { categoriesConfig } from "../../utils/categories.config";

import SectionWrapper from "../../components/SectionWrapper";
import AddCategoryModal from "../../components/AddCategoryModal";

export default function HomePage() {
  const [categories, setCategories] = useState(categoriesConfig);
  const [editModalVisible, setEditModalVisible] = useState(false);
  const [modalCatId, setModalCatId] = useState(null);

  const addCatClickHandler = (catId) => {
    setModalCatId(catId);
    setEditModalVisible(true);
  }

  const renderCategoriesRecursively = categoriesArr => {
    return (
      <ul>
        {categoriesArr.map(cat =>
          <li key={cat.id}>
            {cat.category}
            <button type="button" onClick={() => {addCatClickHandler(cat.id)}}>+</button>
            {cat.subcategories && cat.subcategories.length > 0
              ? renderCategoriesRecursively(cat.subcategories)
              : null}
          </li>
        )}
      </ul>
    );
  };

  const getCategoriesIteratively = (categoriesArr) => {
    const parentElement = Object.assign({}, categoriesArr);
    parentElement.depth = 0;
    parentElement.next = null;
  
    let children, child, i, len, depth, current = parentElement[0];
    while (current.subcategories.length > 0) {
      depth = current.depth;
      children = current.subcategories.length > 0 ? current.subcategories : [];
      for (i = 0, len = children.length; i < len; i++) {
        child = children[i];
        child.depth = depth+1;
        current = child;
        return <li>{current.category}</li>
      }
    }
  }

  const addCateogry = (value) => {
    setEditModalVisible(false);
  }

  return (
    <>
      <SectionWrapper>
        <h1>Recursive rendering mode</h1>
        {renderCategoriesRecursively(categories)}
      </SectionWrapper>

      <SectionWrapper>
        <h1>Iterative rendering mode</h1>
        {getCategoriesIteratively(categories)}
      </SectionWrapper>

      <AddCategoryModal isVisible={editModalVisible} cancelHandler={() => {setEditModalVisible(false)}} submitHandler={addCateogry} />
    </>
  );
}
