/**
 *
 * AddCategoryDropdown
 *
 */

 import { Menu, Dropdown, Button } from 'antd';

function AddCategoryDropdown({ addCategory, addSubcategory }) {
  const menu = (
    <Menu>
      <Menu.Item key="1" onClick={addCategory}>
        Add category
      </Menu.Item>
      <Menu.Item key="2" onClick={addSubcategory}>
        Add subcategory
      </Menu.Item>
    </Menu>
  );

  return (
    <Dropdown overlay={menu}>
      <Button>
        +
      </Button>
    </Dropdown>
  );
}

AddCategoryDropdown.propTypes = {};

export default AddCategoryDropdown;
