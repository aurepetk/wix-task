import Axios from 'axios';

const baseURL = process.env.API_URL;
const tokenKey = 'TwisterToken';

const client = Axios.create({
  baseURL,
  headers: { 'Content-Type': 'application/json' },
});

client.interceptors.request.use(
  config => {
    const token = localStorage.getItem(tokenKey);
    if (token) {
      return {
        ...config,
        headers: {
          ...config.headers,
          Authorization: token,
        },
      };
    }
    return config;
  },
  e => Promise.reject(e),
);

const request = function(options) {
  const onSuccess = function(response) {
    console.debug('Request Successful!', response);
    return response.data;
  };

  const onError = function(error) {
    console.error('Request Failed:', error.config);

    if (error.response && error.response.data.status_code !== 404) {
      // Request was made but server responded with something
      // other than 2xx OR 404
      console.error('Status:', error.response.status);
      console.error('Data:', error.response.data);
      console.error('Headers:', error.response.headers);
    } else {
      // Something else happened while setting up the request
      // triggered the error
      console.error('Error Message:', error.message);
    }

    return Promise.reject(error.response || error.message);
  };

  return client(options)
    .then(onSuccess)
    .catch(onError);
};

export default request;
