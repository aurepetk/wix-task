/**
 *
 * SectionWrapper
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Wrapper from './Wrapper';

function SectionWrapper({ children }) {
  return <Wrapper>{children}</Wrapper>;
}

SectionWrapper.propTypes = {
  children: PropTypes.any,
};

export default SectionWrapper;
