/**
 *
 * AddCategoryModal
 *
 */

import React from 'react';
import { Modal, Input, Button } from 'antd';

function AddCategoryModal({ isVisible, submitHandler, cancelHandler }) {
  const [catName, setCatName] = React.useState("");

  const handleChange = value => {
    setCatName(value);
  };

  return (
    <Modal
      title="Create a new SIBLING category"
      visible={isVisible}
      onOk={() => {submitHandler(catName)}}
      onCancel={cancelHandler}
    >
      <Input placeholder="Add category name" onChange={handleChange} />
    </Modal>
  );
}

AddCategoryModal.propTypes = {};

export default AddCategoryModal;
