import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 60px 40px;
`;

export default Wrapper;