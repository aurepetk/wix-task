export const categoriesConfig = [
    {
        id: "1",
        category: "Movies",
        subcategories: [
            {
                id: "1.1",
                category: "Action",
                subcategories: [
                    { 
                        id: "1.1.1",
                        category: "War and Military" 
                    },
                    { 
                        id: "1.1.2",
                        category: "Martial Arts" 
                    }
                ]
            }
        ]
    }
];